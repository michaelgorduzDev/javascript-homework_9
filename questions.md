1. Опишіть, як можна створити новий HTML тег на сторінці.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
const div = document.createElement("div");
div.innerHTML = "test";
document.body.appendChild(div);


2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
insertAdjacentHTML takes 2 parameters. 1rst parameter is a position, where we will insert our HTML.

Variants are:
beforebegin - before opening tag
afterbegin - after openong tag
beforeend - before closing tag
afterend - after closing tag

2nd parameter is our HTML or text


3. Як можна видалити елемент зі сторінки?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
a) All browsers supported:
let element = document.getElementById("someElement");
element.parentNode.removeChild("someElement");

b) New method - not all browser supported:
let element = document.getElementById("someElement");
element.remove();