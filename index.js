let array1 = ["hello", "Kiev", "Kharkiv", "Odessa"];
let array2 = ["2", "3", "sea", "user", 23];
let multiArray = ["Rome", "Summer", array1, "London", "Paris", array2, "Tokyo"];
const elementLocation = document.body;
const countdownLocation = document.querySelector(".countdown");

let timeContainer = document.createElement("p");
countdownLocation.appendChild(timeContainer);

let time = 3;
const countdown = setInterval(() => {
  timeContainer.innerHTML = time;
  time--;

  if (time < 0) {
    clearInterval(countdown);
    document.body.innerHTML = "";
  }
}, 1000);


function showArray(array, location) {
    const ul = document.createElement('ul');
    location.prepend(ul);
  
    array.forEach((element) => {
      const li = document.createElement('li');
      ul.appendChild(li);
  
      if (Array.isArray(element)) {
        showArray(element, li);
      } else {
        li.textContent = element;
      }
    });
  }
  
  showArray(multiArray, elementLocation);
